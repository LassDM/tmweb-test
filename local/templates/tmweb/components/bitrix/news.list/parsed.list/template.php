<?php
use Bitrix\Main\Config\Option;
/**
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var CBitrixComponent $component
 * @var CBitrixComponentTemplate $this
 * @var \CMain $APPLICATION
 * @var \CUser $USER
 * @var \CDatabase $BD
 * @var $arParams
 * @var $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="sites-list">
	<?if (empty($arResult["ITEMS_BY_SITE"])):?>
		<p>Вы еще не спарсили ни одного сайта!</p>
	<?else:?>
		<?foreach ($arResult["ITEMS_BY_SITE"] as $site => $arSitePages):?>
			<div class="sites-list__item">
				<a href="javascript:void(0);" class="sites-list__item-link js-site-link"><?=$site?></a>
				<div class="sites-list__item-results">
					<?foreach ($arSitePages as $arPage):?>
						<div class="item-results__page">
							<a href="javascript:void(0);" class="item-results__page-link js-show-info" target="_blank"><?=$arPage["NAME"]?></a>
							<?if ($arPage["PROPERTIES"]["IMG_COUNT"]["VALUE"]):?>
								<span class="item-results__page-count"><b>Изображений:&nbsp;</b><?=$arPage["PROPERTIES"]["IMG_COUNT"]["VALUE"]?>;</span>
							<?endif;?>
							<?if ($arPage["PROPERTIES"]["LINK_COUNT"]["VALUE"]):?>
								<span class="item-results__page-count"><b>Ссылок:&nbsp;</b><?=$arPage["PROPERTIES"]["LINK_COUNT"]["VALUE"]?>;</span>
							<?endif;?>
							<?if ($arPage["PROPERTIES"]["ENTRIES_COUNT"]["VALUE"]):?>
								<?foreach ($arPage["PROPERTIES"]["ENTRIES_COUNT"]["VALUE"] as $k => $val):?>
									<span class="item-results__page-count"><b>"<?=$val?>":&nbsp;</b><?=$arPage["PROPERTIES"]["ENTRIES_COUNT"]["DESCRIPTION"][$k]?>;</span>
								<?endforeach;?>
							<?endif;?>
							<div class="item-results__page-more">
								<?if (!$arPage["PREVIEW_TEXT"] && !$arPage["DETAIL_TEXT"]):?>
									<div class="results-more">
										В результате парсинга ничего не найдено.
									</div>
								<?endif;?>
								<?if ($arPage["PREVIEW_TEXT"]):?>
									<div class="results-more">
										<b>Список изображений:</b><br/><?=str_replace("|", "<br/>", $arPage["PREVIEW_TEXT"])?>
									</div>
								<?endif;?>
								<?if ($arPage["DETAIL_TEXT"]):?>
									<div class="results-more">
										<b>Список ссылок:</b><br/><?=str_replace("|", "<br/>", $arPage["DETAIL_TEXT"])?>
									</div>
								<?endif;?>
							</div>
						</div>
					<?endforeach;?>
				</div>
			</div>
		<?endforeach;?>
	<?endif;?>
</div>
