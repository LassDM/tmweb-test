<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// раскидаем ссылки по сайтам
$arItemsBySites = array();
foreach ($arResult["ITEMS"] as $key => $arItem) {
	$domain = parse_url($arItem["NAME"], PHP_URL_HOST);
	$arItemsBySites[$domain][] = $arItem;
}
ksort($arItemsBySites);
$arResult["ITEMS_BY_SITE"] = $arItemsBySites;