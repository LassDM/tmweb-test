"use strict";

$(document).ready(function () {

    $('.js-site-link').on('click', function(){
        $(this).toggleClass('sites-list__item-link_active');
        $(this).parent().toggleClass('sites-list__item_open');
    });
    $('.js-show-info').on('click', function(){
        $(this).parent().find('.item-results__page-more').toggleClass('item-results__page-more_open');
    });

});