<?php

namespace Salerman\Tmweb;

class Parser {

	const foundDelimiter = '|';
	const parserIblockID = 1;

	public $url;
	public $pageContent;
	public $connStatus;
	public $parseType;
	public $parseElementCount;
	public $parseElementString;
	private $page;

	public function __construct() {
		$this->url = '';
		$this->pageContent = '';
		$this->connStatus = '';
		$this->parseType = '';
		$this->parseElementCount = 0;
		$this->parseElementString = '';
	}

	/**
	 * Получить страницу
	 * @param string $url
	 * @return void
	 */
	private function getPageContent($url) {
		$this->url = trim($url);
		$curl = new Mycurl($this->url,true,60,4,false);
		$curl->createCurl();
		$this->page = $curl;
		$this->connStatus = $curl->getHttpStatus();
		$this->pageContent = $curl->__toString();
	}

	/**
	 * Сохраняем текущий результат парсинга в ИБ
	 * @return bool
	 */
	private function saveParseResult() {

		if (!$this->url) return false;

		\CModule::includeModule('iblock');
		$res = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID"=>self::parserIblockID, "NAME"=>$this->url),
			false,
			array("nPageSize"=>1),
			array("ID", "NAME", "PROPERTY_IMG_COUNT", "PROPERTY_LINK_COUNT"));
		if($ob = $res->GetNextElement())
		{
			// элемент существует, обновляем поля
			$arFields = $ob->GetFields();
			$PRODUCT_ID = $arFields["ID"];

			$el = new \CIBlockElement;
			$arLoadProductArray = Array(
				"PREVIEW_TEXT" => ($this->parseType == 'image') ? $this->parseElementString : $arFields["PREVIEW_TEXT"],
				"DETAIL_TEXT" => ($this->parseType == 'link') ? $this->parseElementString : $arFields["DETAIL_TEXT"]
			);
			if ($el->Update($PRODUCT_ID, $arLoadProductArray)) {
				// обновляем также свойство с количеством
				switch ($this->parseType) {
					case "image":

						// если кол-во совпадает, ничего не делаем
						if (intval($arFields["PROPERTY_IMG_COUNT_VALUE"]) != $this->parseElementCount) {
							\CIBlockElement::SetPropertyValuesEx($PRODUCT_ID, false, array("IMG_COUNT" => $this->parseElementCount));
						}
						break;
					case "link":

						// если кол-во совпадает, ничего не делаем
						if (intval($arFields["PROPERTY_LINK_COUNT_VALUE"]) != $this->parseElementCount) {
							\CIBlockElement::SetPropertyValuesEx($PRODUCT_ID, false, array("LINK_COUNT" => $this->parseElementCount));
						}
						break;
					case "text":

						// тут логика несколько сложнее:
						// 1. получаем текущее значение свойства
						// 2. проверяем, нет ли совпадений. если есть, обновляем их кол-во. если кол-во совпадает, ничего не делаем
						// 3. если совпадений нет, дописываем новое поле в свойство

						$arSometextProp = array();
						$bPropUpd = false;
						$res = \CIBlockElement::GetProperty(self::parserIblockID, $PRODUCT_ID, "sort", "asc", array("CODE" => "ENTRIES_COUNT"));
						while ($ob = $res->GetNext())
						{
							// если значение свойства уже есть в списке, обновляем его количество
							if (strtolower($this->parseElementString) == strtolower($ob['VALUE'])) {
								$ob['DESCRIPTION'] = $this->parseElementCount;
								$bPropUpd = true;
							}
							$arSometextProp[] = array(
								"VALUE" => $ob['VALUE'],
								"DESCRIPTION" => $ob['DESCRIPTION']
							);
						}
						// если свойства нет в списке, добавляем его
						if (!$bPropUpd) {
							$arSometextProp[] = array(
								"VALUE" => $this->parseElementString,
								"DESCRIPTION" => $this->parseElementCount
							);
						}
						\CIBlockElement::SetPropertyValuesEx($PRODUCT_ID, false, array("ENTRIES_COUNT" => $arSometextProp));
						break;
				}
				return true;
			}

		} else {
			// такого элемента еще нет - сохраняем заново
			$el = new \CIBlockElement;
			$arLoadProductArray = Array(
				"MODIFIED_BY"    => 1,
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID"      => self::parserIblockID,
				"NAME"           => $this->url,
				"ACTIVE"         => "Y",
				"PREVIEW_TEXT"   => ($this->parseType == 'image') ? $this->parseElementString : "",
				"DETAIL_TEXT"    => ($this->parseType == 'link') ? $this->parseElementString : ""
			);

			if($PRODUCT_ID = $el->Add($arLoadProductArray)) {

				// обновляем также свойство с количеством
				switch ($this->parseType) {
					case "image":
						\CIBlockElement::SetPropertyValuesEx($PRODUCT_ID, false, array("IMG_COUNT" => $this->parseElementCount));
						break;
					case "link":
						\CIBlockElement::SetPropertyValuesEx($PRODUCT_ID, false, array("LINK_COUNT" => $this->parseElementCount));
						break;
					case "text":
						$PROP_VALUE = array(
							0 => array("VALUE"=>$this->parseElementString,"DESCRIPTION"=>$this->parseElementCount)
						);
						\CIBlockElement::SetPropertyValuesEx($PRODUCT_ID, false, array("ENTRIES_COUNT" => $PROP_VALUE));
						break;
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * Парсим указанный url, ищем тип $type
	 * @param string $url
	 * @param string $type(image|link|text)
	 * @param string $sometext|''
	 * @return bool
	 */
	public function parseUrl($url, $type = 'image', $sometext = '') {
		$this->getPageContent($url);
		if ($this->connStatus == 200) {
			$this->parseType = $type;
			switch ($type) {
				case "image":
					$pattern = '/<img[^>]* src=\"([^\"]*)\"[^>]*>/i';
					preg_match_all($pattern,$this->pageContent, $result);
					$this->parseElementCount = count($result[1]);
					$this->parseElementString = implode(self::foundDelimiter, $result[1]);
					break;
				case "link":
					$pattern = '/<a(.+)href=\"([^\"]*)\"(.+)<\/a>/iU';
					preg_match_all($pattern,$this->pageContent, $result);
					foreach ($result[2] as $k => $val) {
						if ($val{0} == '#' || strstr($val, 'tel:') || strstr($val, 'mailto:') || strstr($val, 'javascript:')) {
							unset($result[2][$k]);
						}
					}
					$this->parseElementCount = count($result[2]);
					$this->parseElementString = implode(self::foundDelimiter, $result[2]);
					break;
				case "text":
					// Упс: вариант с '/regexp/i' в данном случае не сработает...
					$sometext = strtolower($sometext);
					$pattern = "/$sometext/";
					// очистим от тегов, чтобы искать только вхождения в текст
					$onlyText = strtolower(strip_tags($this->pageContent));
					preg_match_all($pattern, $onlyText, $result);
					$this->parseElementCount = count($result[0]);
					$this->parseElementString = $sometext;
					break;
			}
			// возвращает true/false под возможное расширение: выводить ошибку, если записи найдены но сохранение в БД не удалось
			return $this->saveParseResult();
		}
	}
}