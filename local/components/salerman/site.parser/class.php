<?php

use Bitrix\Main;
use Bitrix\Main\Config;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\Data;
use Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

class SiteParser extends \CBitrixComponent {

	public function executeComponent()
	{
		$this->includeComponentTemplate();
	}

	protected static function getClassName()
	{
		return __CLASS__;
	}
}
