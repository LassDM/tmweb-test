<?
use Bitrix\Main\Loader,
	Salerman\Tmweb;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/json; charset=utf-8');

if (!Loader::includeModule("salerman.tmweb")) {

	$result = array(
		"RESULT" => false,
		"MESSAGE" => "Не установлен модуль 'salerman.tmweb'. Поиск невозможен."
	);
	echo json_encode($result);
	die();
}

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$arPost = $request->getPostList()->toArray();

if ($arPost["TYPE"] == "text" && empty($arPost["SOMETEXT"])) {
	$result = array(
		"RESULT" => false,
		"MESSAGE" => "Вы должны ввести текст, который собираетесь искать на указанной странице."
	);
	echo json_encode($result);
	die();
}

$patter = '/^(ftp|http|https):\/\/([a-z0-9]+\.)+?/';
if (preg_match($patter, $arPost["URL"], $matches) === 0) {
	$result = array(
		"RESULT" => false,
		"MESSAGE" => "Проверьте введенный адрес - вероятно, он не является валидным. Не забудьте указать протокол."
	);
	echo json_encode($result);
	die();
}

$parser = new \Salerman\Tmweb\Parser();
$parser->parseUrl($arPost["URL"], $arPost["TYPE"], $arPost["SOMETEXT"]);
if ($parser->connStatus !== 200) {
	$result = array(
		"RESULT" => false,
		"MESSAGE" => ($parser->connStatus === 0)
						? "Указанный url, похоже, не существует или защищен от парсинга. Попробуйте указать другой url."
						: "Ошибка! Указанный url вернул ответ \"".$parser->connStatus."\". Попробуйте указать другой url."
	);
	echo json_encode($result);
	die();
}

// возвращаем результат
$result = array(
	"RESULT" => true,
	"MESSAGE" => "",
	"DATA" => array(
		"COUNT" => $parser->parseElementCount,
	),
);
echo json_encode($result);
die();