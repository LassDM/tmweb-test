<?php
use Bitrix\Main\Config\Option;
/**
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var CBitrixComponent $component
 * @var CBitrixComponentTemplate $this
 * @var \CMain $APPLICATION
 * @var \CUser $USER
 * @var \CDatabase $BD
 * @var $arParams
 * @var $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="site-parser" id="site-parser" xmlns:v-bind="http://www.w3.org/1999/xhtml"
	 xmlns:v-on="http://www.w3.org/1999/xhtml">
	<ul class="nav nav-tabs site-parser__types">
		<li v-for="(type, index) in parseTypes" class="site-parser__type-item js-type"
			:class="{ 'active':type.default }">
			<a href="#" class="js-in-tab"
			   @click.prevent="onTypeClick"
				:id="type.val">
				{{ type.name }}
			</a>
		</li>
	</ul>
	<div class="site-parser__controls">
		<input type="text" class="site-parser__site js-url"
			   placeholder="Введите ссылку на сайт, например https://google.com"
			   v-model="url"
			   @keyup.enter="submitForm"
			   @input="onTyping"
			/>
		<button class="site-parser__action-btn" @click.prevent="submitForm">Парсить!</button>
		<input type="text" class="site-parser__site_sometext"
			   placeholder="Введите произвольный текст"
			   v-model="sometext"
			   @keyup.enter="submitForm"
			   @input="onTyping"
			   v-if="currentTab == 'text'"
			/>
		<div class="site-parser__wrapper-text site-parser__loading" v-if="loadingProcess" >
			Работаем...
		</div>
		<div class="site-parser__wrapper-text site-parser__error" v-if="hasError" >
			{{ errMessage }}
		</div>
		<div class="site-parser__wrapper-text site-parser__result" v-if="hasResult" >
			{{ resMessage }}
		</div>
	</div>
</div>