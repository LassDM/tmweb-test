"use strict";

$(document).ready(function () {
	var parser = new Vue({
		el: "#site-parser",
		data: {
			parseTypes: [
				{
					'val': 'image',
					'name': 'Искать изображения',
					'default': true
				},
				{
					'val': 'link',
					'name': 'Искать ссылки',
					'default': false
				},
				{
					'val': 'text',
					'name': 'Искать произвольный текст',
					'default': false
				},
			],
			url: '',
			sometext: '',
			hasError: false,
			errMessage: '',
			hasResult: false,
			resMessage: '',
			loadingProcess: false,
			currentTab: 'image',
		},
		methods: {
			onTyping: function() {
				if (this.hasError) {
					this.hasError = false;
				}
				if (this.hasResult) {
					this.hasResult = false;
				}
			},
			onTypeClick: function(e) {
				if (this.loadingProcess) {
					return;
				}
				this.hasResult = false;
				$('.js-in-tab').parent().removeClass('active');
				$('#'+e.target.id).parent().addClass('active');
				this.currentTab = e.target.id;
			},
			submitForm: function() {
				if (this.loadingProcess) {
					return;
				}
				this.validateForm();
				if (!this.hasError) {
					this.loadingProcess = true;
					var $thisVue = this;
					$.ajax({
						url: "/local/components/salerman/site.parser/ajax.php",
						type: "POST",
						data: {
							TYPE: $thisVue.currentTab,
							URL: $thisVue.url,
							SOMETEXT: $thisVue.sometext
						},
						success: function(result){
							$thisVue.loadingProcess = false;
							if (result.RESULT == false) {
								$thisVue.errMessage = result.MESSAGE;
								$thisVue.hasError = true;
							} else {
								(result.DATA.COUNT === 0)
									? $thisVue.resMessage = 'По вашему запросу на странице ничего не найдено.'
									: $thisVue.resMessage = 'Количество вхождений по вашему запросу: '+result.DATA.COUNT;
								$thisVue.hasResult = true;
							}
						},
						dataType: "json"
					});
				}
			},
			validateForm: function() {
				this.hasError = false;
				// проверка урла на содержание строки
				var reg = /^(ftp|http|https):\/\/([a-z0-9]+\.)+?/;
				if(!reg.test(this.url)) {
					this.errMessage = 'Проверьте введенный адрес - вероятно, он не является валидным. Не забудьте указать протокол.';
					this.hasError = true;
				}
				// проверка поля произвольного текста на заполнение
				if (this.currentTab == 'text' && this.sometext == '') {
					if (this.hasError) {
						this.errMessage += ' Кроме того, вы должны ввести текст, который собираетесь искать на указанной странице.';
					} else {
						this.errMessage = 'Вы должны ввести текст, который собираетесь искать на указанной странице.';
						this.hasError = true;
					}
				}
			}
		}
	});
});